import Vue from "vue";

let lastId = 5;

const state = {
    users: [
        {
            id: 1,
            name: "Nicholas Runolfsdottir",
            email: "Sherwood@rosamond.me",
            avatar: "https://randomuser.me/api/portraits/med/men/35.jpg",
        },
        {
            id: 2,
            name: "Glenna Reichert",
            email: "Chaim_McDermott@dana.io",
            avatar: "https://randomuser.me/api/portraits/med/women/60.jpg",
        },
        {
            id: 3,
            name: "Clementina DuBuque",
            email: "Rey.Padberg@karina.biz",
            avatar: "https://randomuser.me/api/portraits/med/women/28.jpg",
        },
        {
            id: 4,
            name: "Patricia Lebsack",
            email: "Julianne.OConner@kory.org",
            avatar: "https://randomuser.me/api/portraits/med/women/73.jpg",
        },
        {
            id: 5,
            name: "Ervin Howell",
            email: "Shanna@melissa.tv",
            avatar: "https://randomuser.me/api/portraits/med/women/29.jpg",
        },
    ],
};

const getters = {
    countFilterUsers: state => (searchType, keyword) => {
        if (!keyword) {
            return state.users.length;
        } else {
            let filterUsersArray =  state.users.filter(function (elem) {
                if (searchType === "email") {
                    return elem.email.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                } else if (searchType === "name") {
                    return elem.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                }
            });
            return filterUsersArray.length;
        }
    },

    filterUsers: state => (searchType, keyword) => {
        if (!keyword) {
            return state.users;
        } else {
            return state.users.filter(function (elem) {
                if (searchType === "email") {
                    return elem.email.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                } else if (searchType === "name") {
                    return elem.name.toLowerCase().indexOf(keyword.toLowerCase()) > -1;
                }
            });
        }
    }
};

const mutations = {
    ADD_USER(state, user) {
        state.users.push({
            id: ++lastId,
            name: user.name,
            email: user.email,
            avatar: user.avatar
        });
    },

    EDIT_USER(state, { userId, data }) {
        const index = state.users.findIndex(user => user.id === userId);

        if (index !== -1) {
            const updatedUser = {
                id: userId,
                name: data.name,
                email: data.email,
                avatar: data.avatar
            };

            Vue.set(state.users, index, updatedUser);
        }
    },

    DELETE_USER(state, userId) {
        const index = state.users.findIndex(user => user.id === userId);

        if (index !== -1) {
            state.users.splice(index, 1);
        }
    }
};

const actions = {
    addUser({ state, commit }, data) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit("ADD_USER", data);
                resolve();
            }, 150);
        });
    },

    editUser({ commit }, data) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit("EDIT_USER", data);
                resolve();
            }, 150);
        });
    },

    deleteUser({ commit }, id) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit("DELETE_USER", id);
                resolve();
            }, 150);
        });
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};