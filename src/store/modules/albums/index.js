import Vue from "vue";

let lastId = 20;

const state = {
    albums: [
        {
            "userId": 1,
            "id": 1,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "quidem molestiae enim"
        },
        {
            "userId": 1,
            "id": 2,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "sunt qui excepturi placeat culpa"
        },
        {
            "userId": 1,
            "id": 3,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "omnis laborum odio"
        },
        {
            "userId": 1,
            "id": 4,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "non esse culpa molestiae omnis sed optio"
        },
        {
            "userId": 1,
            "id": 5,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "eaque aut omnis a"
        },
        {
            "userId": 2,
            "id": 6,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "natus impedit quibusdam illo est"
        },
        {
            "userId": 2,
            "id": 7,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "quibusdam autem aliquid et et quia"
        },
        {
            "userId": 2,
            "id": 8,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "qui fuga est a eum"
        },
        {
            "userId": 2,
            "id": 9,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "saepe unde necessitatibus rem"
        },
        {
            "userId": 3,
            "id": 10,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "distinctio laborum qui"
        },
        {
            "userId": 3,
            "id": 11,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "quam nostrum impedit mollitia quod et dolor"
        },
        {
            "userId": 3,
            "id": 12,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "consequatur autem doloribus natus consectetur"
        },
        {
            "userId": 4,
            "id": 13,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "ab rerum non rerum consequatur ut ea unde"
        },
        {
            "userId": 4,
            "id": 14,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "ducimus molestias eos animi atque nihil"
        },
        {
            "userId": 5,
            "id": 15,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "ut pariatur rerum ipsum natus repellendus praesentium"
        },
        {
            "userId": 5,
            "id": 16,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "voluptatem aut maxime inventore autem magnam atque repellat"
        },
        {
            "userId": 5,
            "id": 17,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "aut minima voluptatem ut velit"
        },
        {
            "userId": 5,
            "id": 18,
            "preview": "https://via.placeholder.com/600/149540",
            "title": "nesciunt quia et doloremque"
        },
        {
            "userId": 5,
            "id": 19,
            "preview": "https://via.placeholder.com/600/4e557c",
            "title": "velit pariatur quaerat similique libero omnis quia"
        },
        {
            "userId": 5,
            "id": 20,
            "preview": "https://via.placeholder.com/600/cd5a92",
            "title": "voluptas rerum iure ut enim"
        },
    ]
};

const getters = {
    getUserAlbums: state => userId => {
        if (!userId) {
            return state.albums;
        }

        return state.albums.filter((album) => album.userId === userId);
    },
    amountUserAlbums: state => userId => {
        if (!userId) {
            return state.albums.length;
        }

        return state.albums.filter((album) => album.userId === userId).length;
    }
};

const mutations = {
    ADD_ALBUM(state, album) {
        state.albums.push({
            id: ++lastId,
            title: album.title,
            preview: album.preview,
            userId: album.userId
        });
    },

    DELETE_ALBUM(state, albumId) {
        const index = state.albums.findIndex(album => album.id === albumId);

        if (index !== -1) {
            state.albums.splice(index, 1);
        }
    },

    EDIT_ALBUM(state, { albumId, data }) {
        const index = state.albums.findIndex(album => album.id === albumId);

        if (index !== -1) {
            const updatedAlbum = {
                id: albumId,
                title: data.title,
                preview: data.preview,
                userId: data.userId
            };

            Vue.set(state.albums, index, updatedAlbum);
        }
    },
};

const actions = {
    addAlbum({ state, commit }, data) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit("ADD_ALBUM", data);
                resolve();
            }, 150);
        });
    },

    deleteAlbum({ commit }, albumId) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit("DELETE_ALBUM", albumId);
                resolve();
            }, 150);
        });
    },

    editAlbum({ commit }, data) {
        return new Promise(resolve => {
            setTimeout(() => {
                commit('EDIT_ALBUM', data);
                resolve();
            }, 150);
        });
    },
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
};